//gulp stored
var gulp = require('gulp');

const sass = require('gulp-sass'),
      cssmini = require('gulp-cssnano'),
      sourceMap = require('gulp-sourcemaps'),
      jsmini = require('gulp-uglify'),
      imagemini = require('gulp-imagemin'),
      clear = require('del'),
      minilink = require('gulp-useref'),
      live = require('browser-sync').create(),
      orderIn = require('run-sequence'),
      cache = require('gulp-cache'),
      htmlmini = require('gulp-htmlmin');

const PATH = {
  src : 'assets',
  dest : 'dist'
}





// from sass to css
gulp.task('sass',function() {
	return gulp.src(PATH.src+'/sass/**/*.scss')
					.pipe(sass())
					.pipe(gulp.dest(PATH.src+'/css'))
          .pipe(live.reload({
            stream : true
          }));
});

//from css to mini css
gulp.task('minicss',function() {
  return gulp.src(PATH.src+'/css/*')
          .pipe(sourceMap.init())
          .pipe(cssmini())
          .pipe(sourceMap.write())
          .pipe(gulp.dest(PATH.dest+'/assets/css/'))
});

// to minify js
gulp.task('minijs',function() {
  return gulp.src(PATH.src+'/js/*.js')
          .pipe(sourceMap.init())
          .pipe(jsmini())
          .pipe(sourceMap.write())
          .pipe(gulp.dest(PATH.dest+'/assets/js/'))
          .pipe(live.reload({
            stream: true
          }))
});

// to reload browser live
gulp.task('live',function() {
    live.init({
      server : { 
        baseDir : PATH.dest
      }
  });
});


// to watch all files
gulp.task('watchall',['live'],function() {
  gulp.watch(PATH.src+'/sass/**/*.scss',['sass','minicss']);
  gulp.watch(PATH.src+'/js/**/*.js',live.reload);
  gulp.watch(PATH.src+'*.html',live.reload);
  gulp.watch('assets/pages/**/*.html',['nunjucks']).on('change', live.reload);
  gulp.watch('assets/templates/**/*.html',['nunjucks']).on('change', live.reload);
  gulp.watch('*.html',live.reload);
});

// to copy images
gulp.task('font',function() {
  return gulp.src(PATH.src+'/vendor/**', {"base":"."})
         .pipe(gulp.dest(PATH.dest))
         .pipe(live.reload({
            stream: true
          }));
});


//to copy html to dist
gulp.task('html',function() {
  return gulp.src('*.html')
         .pipe(htmlmini({collapseWhitespace:true}))
         .pipe(gulp.dest(PATH.dest))
         .pipe(live.reload({
            stream: true
          }));
});


// to minify images
gulp.task('image-minify',function() {
  return gulp.src(PATH.src+'/images/**/*.+(png|jpg|gif|svg|jpeg)')
          .pipe(imagemini({
            interlace : true
          }))
          .pipe(gulp.dest(PATH.dest+'/assets/images'))
          .pipe(live.reload({
            stream: true
          }));
});

// to clear dist folder
gulp.task('clean:dist',function() {
  return clear.sync('dist');
})

// to build project
gulp.task('build',function(callback) {
  orderIn('clean:dist',['sass','image-minify','minicss','minijs','font','html','watchall'],callback)
}); 