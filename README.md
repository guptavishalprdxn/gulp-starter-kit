# Requirement
  - Node: [https://nodejs.org/en/download] 
  
# How to Start
1.  Download & unzip: [https://bitbucket.org/guptavishalprdxn/gulp-starter-kit/get/59a1c008410f.zip](https://bitbucket.org/guptavishalprdxn/gulp-starter-kit/get/59a1c008410f.zip)

2.  Step to follow: 
    ~~~sh
       $ cd gulp-starter-kit
    ~~~
3.  Install dependencies: -
 - To run gulp command - gulp-package
   ```sh
        $ npm gulp install --save-d
   ```
 - To live reload in browser -  browser-sync
    ```sh
        $ npm browser-sync install --save-d
    ```
 - To minify HTML - gulp-htmlmin
    ```sh
        $ npm gulp-htmlmin install --save-d
    ```
 - To minify CSS -  gulp-cssnano
    ```sh
        $ npm gulp-cssnano install --save-d
    ```
 - To convert sass to css -  gulp-sass
    ```sh
        $ npm gulp-sass install --save-d
    ```
 - To minify Image - gulp-imagemin
    ```sh
        $ npm gulp-imagemin install --save-d
    ```
 - To minify JS   - gulp-uglify
    ```sh
        $ npm gulp-uglify install --save-d
    ```
 - To cache image gulp-cache
    ```sh
        $ npm gulp-cache install --save-d
    ```
 - To run task in sequence - run-sequence
    ```sh
        $ npm run-sequence install --save-d
    ```
 - To clear directory -  del
    ```sh
        $ npm del install --save-d
    ```
 
4.  Run gulp 
    ```
        $ gulp build
    ```
4. Try change something inside index.html & your browser will auto-reload, displaying the change you just made.


# Why?  
 - works out of the box!
 - output is plain html (stored in dist directory)
 - serverless
 - minimum knowledge needed: html, css, js  no need for php, ruby, etc.
 - minifier included! 
# Directory Layout

Here is the project structure:
-  dist : this directory contains real files that will be hosted
- dist/assets : all css, js, images, fonts and whatever assets related to app are  located here
- assets/ : raw files used to develop the app

# Sample workflow

 A. with auto-render & auto-reload (via browsersync) :
~~~sh
   $ gulp build
~~~
 - it will open http://localhost:3000 in your browser
 - using this workflow, when you edit src the dist will be updated (just like workflow B) and your browser (http://localhost:3000) will be reloaded automatically.

B. with auto-render
```
    $ run gulp watch
```
- as you edit src, dist gulp watch will do the rendering behind the scene n updates dist directory: you need to reload your browser to see the changes
- publish dist to hosting provider
***
# Nunjucks official docs:
- https://mozilla.github.io/nunjucks
# Jinja2  official docs
- [http://jinja.pocoo.org/docs/2.9/](http://jinja.pocoo.org/docs/2.9/)